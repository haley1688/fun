const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}
const BASE_URL = process.env.NODE_ENV === "production" ? "/fun/" : "/";
module.exports = {
  publicPath: BASE_URL,
  productionSourceMap: false,
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("@assets", resolve("src/assets"))
      .set("@components", resolve("src/components"))
      .set("@base", resolve("baseConfig"))
      .set("@public", resolve("public"));
  },
}