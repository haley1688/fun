import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
Vue.use(ViewUI);
import '@/styles/css.scss';
var _ = require('lodash');//工具函数
import {setStore,getStore,clearStore} from '@/libs/storage'
Vue.prototype.setStore = setStore;
Vue.prototype.getStore = getStore;
Vue.prototype.clearStore = clearStore;
// 复制
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)
const echarts = require('echarts');
Vue.prototype.$echarts = echarts

import axios from 'axios'
Vue.prototype.$axios = axios
Vue.prototype.isMobile=(()=> {
  let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)
  return flag;
})()



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
