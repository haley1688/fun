import {base} from "@/libs/base"
// 学习
export const obj = {
    title: '育儿教学',
    label: '小学语文数学教学',
    icon: 'gem-o',
    titleClass: 'titleClass-blue',
    children: [
        {
            title: '加法减法练习题、认识时钟',
            desc: '加法减法乘法除法练习题、比大小、认识时钟、汉字描红、英语描红',
            detail:require('@/assets/imgs/page/study/add.png'),
            link: 'https://www.dayin.page/math/add_sub',
            target:1
        },
    ]
}