import {base} from "@/libs/base"
// 图像视频
export const obj = {
    title: '图像视频',
    label: '在线ps、图片压缩',
    icon: 'photo-o',
    titleClass: 'titleClass-blue',
    children: [
        // {
        //     title: '手机app横向后提示',
        //     link: base+'/orientation',
        //     src:'imgs/or.png',
        // },
        {
            title: '在线ps',
            desc: '在线版photoshop（ps）',
            detail:require('@/assets/imgs/page/img/ps.png'),
            link: 'https://www.uupoop.com/ps/',
            target:1
        },
        {
            title: '小熊猫压缩',
            desc: '不丢失清晰度的前提下压缩png等图像大小',
            detail:require('@/assets/imgs/page/img/yasuo.png'),
            link: 'https://tinypng.com/',
            target:1
        },
        {
            title: 'AI智能图片放大',
            desc: 'AI智能图片放大',
            detail:require('@/assets/imgs/page/img/zoom.png'),
            link: 'https://bigjpg.com/',
            target:1
        },
        {
            title: 'AI改图',
            desc: '图片放大、去水印、一键抠图、gif制作',
            detail:require('@/assets/imgs/page/img/modify.png'),
            link: 'https://img.logosc.cn/',
            target:1
        },
        
    ]
}