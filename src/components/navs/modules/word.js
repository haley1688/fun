import {base} from "@/libs/base"
// 文字
export const obj = {
    title: '文字内容处理',
    label: '',
    icon: 'gem-o',
    titleClass: 'titleClass-blue',
    children: [
        {
            title: '找不同',
            desc: '不同内容进行对比',
            detail:require('@/assets/imgs/page/word/notSame.png'),
            link: 'https://www.jq22.com/textDifference',
            target:1
        },
    ]
}