import {base} from "@/libs/base"
// 资源
export const obj = {
    title: '文件下载、图书查找',
    label: '文件下载、电影下载、图书下载',
    icon: 'gem-o',
    titleClass: 'titleClass-blue',
    children: [
        {
            title: '无名图书',
            desc: '依据书名、作者、ISBN查找书籍',
            detail:require('@/assets/imgs/page/resource/noName.png'),
            link: 'https://www.book123.info/',
            target:1
        },
        {
            title: '鸠摩搜索',
            desc: '各种资源',
            detail:require('@/assets/imgs/page/resource/jiumo.png'),
            link: 'https://www.jiumodiary.com/',
            target:1
        },
        {
            title: '各种格式转化',
            desc: '电子书epub mobi 音频 视频 字体 cad',
            detail:require('@/assets/imgs/page/resource/format.png'),
            link: 'https://convertio.co/zh/epub-mobi/',
            target:1
        },
    ]
}