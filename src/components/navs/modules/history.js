import {base} from "@/libs/base"
// 历史文化
export const obj = {
    title: '历史文化',
    label: '历史朝代、古籍、画作、族谱、小说、书籍',
    icon: 'gem-o',
    titleClass: 'titleClass-blue',
    children: [
        // {
        //     title: '手机app横向后提示',
        //     link: base+'/orientation',
        //     src:'imgs/or.png',
        // },
        {
            title: '全历史在线查看',
            desc: '展示全球各个朝代的历史、人文、画作、古籍、族谱',
            detail:require('@/assets/imgs/page/history/history.png'),
            link: 'https://www.allhistory.com/',
            target:1
        },
        {
            title: '微信读书网页版',
            desc: '各种小说、专业书籍',
            detail:require('@/assets/imgs/page/history/weixin.png'),
            link: 'https://weread.qq.com/',
            target:1
        },
    ]
}