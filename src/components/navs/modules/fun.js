import {base} from "@/libs/base"
// 无聊的时候
export const obj = {
    title: '无聊的时候可以做的事情',
    label: '',
    icon: 'photo-o',
    titleClass: 'titleClass-blue',
    children: [
        {
            title: '比谁找的牛多',
            desc: '需要打开声音，鼠标在屏幕内移动，距离牛越近声音越大',
            detail:require('@/assets/imgs/page/fun/findCow.png'),
            link: 'https://findtheinvisiblecow.com/',
            target:1
        },
    ]
}