import {base} from "@/libs/base"
// 学习
export const obj = {
    title: '各种学习课程',
    label: '学习课程',
    icon: 'gem-o',
    titleClass: 'titleClass-blue',
    children: [
        {
            title: 'oeasy教程网',
            desc: '学习课程有：爱情、电路、系统、编程、前端、平面、影视、动画、音频、游戏、办公、日常',
            detail:require('@/assets/imgs/page/learn/oeasy.png'),
            link: 'http://oeasy.org/',
            target:1
        },
    ]
}