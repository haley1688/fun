import {obj as fun} from './modules/fun'
import {obj as img} from './modules/img'
import {obj as history} from './modules/history'
import {obj as resource} from './modules/resource'
import {obj as word} from './modules/word'
import {obj as learn} from './modules/learn'
import {obj as study} from './modules/study'
export const navs = [
    fun,
    img,
    history,
    resource,
    word,
    learn,
    study,
]