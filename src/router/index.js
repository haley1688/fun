import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    redirect: '/index',
  },
  {
    path: '/index',
    component: Home
  },
  {
    path: '/page',
    component: () => import('../views/Main.vue'),
    children: [
      {
        path: '/test',
        meta: {
          title: '项目内部测试工具',
        },
        component: () => import('@/views/page/test.vue')
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
